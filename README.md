
Setting up Build Pipelines for Java application to support Release activities.














## Description

 - Enable CI build & Full builds on Feature branches: on-Commit, on-Demand & Scheduled builds.

 


## Tech Stack

- **Jenkins**
- **Docker**
- **Jfrog Artifactory**
- **Gitlab**
- **ECR**
- **Jacoco**


## Pipeline
<p align="center">
  ![](images/CI.jpeg)
</p




